Executive Summary
-----------------

Simple contact management application 

Technoloies:
PHP 5.3, LAMP, JQuery, Custom Code, Some styling with Twitter Bootstrap.

Features:
- create sortable table with First Name, Last Name, Country, City, Address, Email
- create add/edit/delete for that table
-  use PHP without a framework
- you can use Java Script or jQuery
- think of the security aspects of this page
- data should be saved in MySql
- project should be ready for live usage
- report back the time that it took you to complete the task


Pending/Todos:

sortable table
delete contact
password encryption
spinner effects on ajax calls
paginations
search
basic required field validations are already applied on first name, last name, email and country - email validation is pending



Login /security:
All contacts operations add/edit/delete , home page views are restricted for admin users
The account is created manually 

The current user credentials are:
username: lokendra3777@gmail.com
password: ****

INSTALL Notes:
------------------------------------------------------------

1) Database setup:
Create database using following database scripts in sequence:

data/db_init.sql
data/db_create.sql

**Notes - the MySQL database and credentials created shall be:
db name: oneprofile  
username: oneprofile
default password: oneprofile123

2) Configurtion files and changes:
For any other users grants We MUST change the Database configurations in:
<project_root>/config.php file

3) Drop the complete project into the HTTP servers document roots:
For example : In apache2 we may copy the project to <apache_root>/htdocs directory

4) Optional** Virtual host configuratios in apache2.conf file:
<VirtualHost *:80>
	ServerName oneprofile.com
	ServerAlias oneprofile.com
	DocumentRoot "C:/xampp/htdocs/oneprofile"
	RewriteEngine On
</VirtualHost>
<VirtualHost *:80>
	ServerName oneprofile.com
	ServerAlias *.oneprofile.com
	DocumentRoot "C:/xampp/htdocs/oneprofile"
	RewriteEngine On
</VirtualHost>




Run Notes:
------------------------------------------------------------------------
Run the url:
localhost/oneprofile/index.php or localhost/oneprofile

It shall take you at the admin login screen.
Enter :
username: lokendra3777@gmail.com
password: ****

It should take you at home page where we can list/add/update/delete contacts











