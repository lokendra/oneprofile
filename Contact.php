<?php


class Contact extends Model{

	public $id;
	public $owner_id;
	public $first_name;
	public $last_name;
	public $email;
	public $country;
	public $city;
	public $address;

	public function validate(){

		if($this->hasErrors()){
			$this->status = self::$STATUS_ERROR;
			return false;
		}


		if(!$this->first_name){
			$this->addError("First Name cannot be blank!");

		}
			
		if(!$this->last_name){
			$this->addError("Last Name cannot be blank!");

		}
			
		if(!$this->email){
			$this->addError("Email cannot be blank!");

		}else{

		}
			
		if(!$this->country){
			$this->addError("Country cannot be blank!");

		}
			
		if($this->hasErrors()){
			$this->status = self::$STATUS_ERROR;
			return false;
		}else{
			$this->status = self::$STATUS_OK;

			return true;
		}
			
			
			
	}


	public static function removeById($id){
		$model = false;

		$db = DbService::getInstance();
		$stmt = $db->prepare("delete from profiles where id = :id");
		$stmt->bindValue(':id', intval($id), PDO::PARAM_INT);
		$stmt->execute();

		$affected_rows = $stmt->rowCount();
		return $affected_rows;

	}

	public static function findById($id){
		$model = false;

		$db = DbService::getInstance();
		$stmt = $db->prepare("select * from profiles where id = :id");
		$stmt->bindValue(':id', intval($id), PDO::PARAM_INT);
		$stmt->execute();
		//$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if($row){
			$model = new Contact();
			$model->setAttributes($row);

		}

		return $model;

	}

	public function setAttributes($params){
		if(!$params){
			$this->addError("Inputs are not valid!");
			$this->status = self::$STATUS_ERROR;


		}

		$this->id = isset($params['id'])?$params['id']:false;
		$this->owner_id = isset($params['owner_id'])?$params['owner_id']:false;
		$this->first_name = isset($params['first_name'])?$params['first_name']:false;
		$this->last_name = isset($params['last_name'])?$params['last_name']:false;
		$this->email = isset($params['email'])?$params['email']:false;
		$this->country = isset($params['country'])?$params['country']:false;
		$this->city = isset($params['city'])?$params['city']:false;
		$this->address = isset($params['address'])?$params['address']:false;

	}



	public  function save($params=false){



		if($this->validate()){
			$db = DbService::getInstance();

			if(!$this->id){
				$stmt = $db->prepare("INSERT INTO profiles (owner_id, first_name, last_name, email, country, city, address)
			      VALUES (:owner_id,:first_name, :last_name, :email, :country, :city, :address)");

				$stmt->bindParam(':owner_id', $this->owner_id, PDO::PARAM_INT);
				$stmt->bindParam(':first_name', $this->first_name, PDO::PARAM_STR);
				$stmt->bindParam(':last_name', $this->last_name, PDO::PARAM_STR);
				$stmt->bindParam(':email', $this->email, PDO::PARAM_STR);
				$stmt->bindParam(':country', $this->country, PDO::PARAM_STR);
				$stmt->bindParam(':city', $this->city, PDO::PARAM_STR);
				$stmt->bindParam(':address', $this->address, PDO::PARAM_STR);
					


				try {
					$affected_rows = $stmt->execute();
					// do other things if successfully inserted
					if($affected_rows > 0){
						$this->status = self::$STATUS_OK;
					}
						
					$this->id = $db->lastInsertId() ;
				} catch (PDOException $e) {
					$this->status = self::$STATUS_ERROR;
					if ($e->errorInfo[1] == 1062) {
						// duplicate entry, do something else

						$this->addError("Contact values :email, or name already exists!");
					} else {
						// an error other than duplicate entry occurred
						$this->addError("Contact save error!");

					}
						
					return;
				}

					
			}else{
				$stmt = $db->prepare("UPDATE profiles SET
				   first_name =:first_name
				   , last_name =:last_name
				   , email=:email
				   , country=:country
				   , city=:city
				   , address=:address
			       where ID =:id");
					
				$stmt->bindParam(':first_name', $this->first_name, PDO::PARAM_STR);
				$stmt->bindParam(':last_name', $this->last_name, PDO::PARAM_STR);
				$stmt->bindParam(':email', $this->email, PDO::PARAM_STR);
				$stmt->bindParam(':country', $this->country, PDO::PARAM_STR);
				$stmt->bindParam(':city', $this->city, PDO::PARAM_STR);
				$stmt->bindParam(':address', $this->address, PDO::PARAM_STR);
				$stmt->bindParam(':id', $this->id, PDO::PARAM_STR);
					
				try {
					$affected_rows = $stmt->execute();
					// do other things if successfully inserted
					if($affected_rows > 0){
						$this->status = self::$STATUS_OK;
					}
				} catch (PDOException $e) {
					$this->status = self::$STATUS_ERROR;
					if ($e->errorInfo[1] == 1062) {
						// duplicate entry, do something else

						$this->addError("Contact values :email, or name is duplicate and MUST be unique!");
					} else {
						// an error other than duplicate entry occurred
						$this->addError("Contact update error!");

					}
						
					return;
				}

			}



		}

	}


	public function findAll($params=array(), $options=array()){

		$contacts = array();

		$model = false;

		$db = DbService::getInstance();

		$query = "select * from profiles ";
		$orderby = "";
		$limit = " limit 1000 ";
		
		if(isset($params['order'])){
			$order = trim($params['order']);
			$direction = isset($params['direction'])?$params['direction']:'ASC';

			if($order && $order!=''){
			
			$orderby = " ORDER BY ".$order ." ".$direction;
			}

			//echo $orderby;
		}
		
		$query .= $orderby." ".$limit;
		
		
		//echo $query;
		$stmt = $db->prepare($query);


		//$stmt->bindValue(':id', intval($id), PDO::PARAM_INT);
		$stmt->execute();
		//$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($rows as $row){
			$model = new Contact();
			$model->setAttributes($row);

			$contacts[] = $model;

		}

		return $contacts;

	}


	public function findAll_dummy($params=array(), $options=array()){

		$contacts = array(
		array('first_name'=>'Lokesh', 'last_name'=>'Kumar', 'email'=>'lokendra3777@gmail.com', 'address'=>'Noida,India')
		,array('first_name'=>'Lokesh1', 'last_name'=>'Kumar', 'email'=>'lokendra3777@gmail.com', 'address'=>'Noida,India')

		);
		return $contacts;
	}


}
