<?php

header('Content-type: application/json');



if(isset($_REQUEST['id']) && $_REQUEST['id']!='' && trim($_REQUEST['id'])!=''){
	$contactModel = Contact::findById($_REQUEST['id']);
	
	if(!$contactModel){
		$status = array('s'=>-1);
        $status['m'] = "No contact found error for contact id: (".$_REQUEST['id'].")";
        
        $resultJson = htmlspecialchars(json_encode($status), ENT_NOQUOTES);
        echo $resultJson;
        return;
	}
	
}else{

   $contactModel = new Contact();
   
   $_REQUEST['owner_id'] = intval($_SESSION['userId']);
   

}

$contactModel->setAttributes($_REQUEST);
$contactModel->save($_REQUEST);

$status = array('s'=>1);
$status['contact'] = $contactModel;


$resultJson = htmlspecialchars(json_encode($status), ENT_NOQUOTES);
echo $resultJson;
return;