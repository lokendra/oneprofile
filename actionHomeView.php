<?php /**
- create sortable table with First Name, Last Name, Country, City, Address, Email
- create add/edit/delete for that table
-  use PHP without a framework
- you can use Java Script or jQuery
- think of the security aspects of this page
- data should be saved in MySql
- project should be ready for live usage
- report back the time that it took you to complete the task

**/?>

<div class="row">
<h3>My Contacts</h3>

<div id="contactWidget">
<div id="status"></div>

<div class="btn-group form-actions2"><a href="#" class="btn add">Add
Contact</a></div>
<hr />

<table id="listArea" class="table table-striped">
	<tr>
		<td>Id&nbsp;<a href="index.php?action=actionHomeView&order=id&direction=ASC" ><i class="icon-arrow-up"></i></a>
		&nbsp;<a href="index.php?action=actionHomeView&order=id&direction=DESC" ><i class="icon-arrow-down"></i></a>
		</td>
		<td>First Name
		&nbsp;<a href="index.php?action=actionHomeView&order=first_name&direction=ASC" ><i class="icon-arrow-up"></i></a>
		&nbsp;<a href="index.php?action=actionHomeView&order=first_name&direction=DESC" ><i class="icon-arrow-down"></i></a>
		</td>
		<td>Last Name&nbsp;<a href="index.php?action=actionHomeView&order=last_name&direction=ASC" ><i class="icon-arrow-up"></i></a>
		&nbsp;<a href="index.php?action=actionHomeView&order=last_name&direction=DESC" ><i class="icon-arrow-down"></i></a></td>
		<td>Country
		&nbsp;<a href="index.php?action=actionHomeView&order=country&direction=ASC" ><i class="icon-arrow-up"></i></a>
		&nbsp;<a href="index.php?action=actionHomeView&order=country&direction=DESC" ><i class="icon-arrow-down"></i></a>
		</td>
		<td>City
		&nbsp;<a href="index.php?action=actionHomeView&order=city&direction=ASC" ><i class="icon-arrow-up"></i></a>
		&nbsp;<a href="index.php?action=actionHomeView&order=city&direction=DESC" ><i class="icon-arrow-down"></i></a>
		</td>
		<td>Address</td>
		<td>Email</td>
		<td>&nbsp;</td>
	</tr>
	<tbody id="listAreaContent">

	</tbody>
</table>

<div id="editArea"></div>

</div>

<hr />
<table id="contactViewTmpl" style="display: none;">
	<tr>
		<td class="id">#id</td>
		<td><span class="first_name"></span></td>
		<td><span class="last_name"></td>
		<td class="country">#country</td>
		<td class="city">#city</td>
		<td class="address">#address</td>
		<td class="email">#email</td>
		<td>
		<div class="btn-group"><a href="#" class="btn edit">Edit</a> <a
			href="#" class="btn remove">Remove</a></div>
		</td>
	</tr>
</table>

<div id="contactEditTmpl" style="display:none;">

<div class="well content">
<h3 id="title"></h3>
<hr />
<div id="editStatus" class="alert alert-error" style="display: none">
<ul>

</ul>
</div>

<form action="index.php" method="get" class="well">
<fieldset><legend></legend> 

<input type="hidden" name="id" value="">
<input type="hidden" name="action" value="actionContactEdit">
<input type="hidden" name="format" value="json">

<div class="row2"><label>* First Name:</label> <input type="text"
	name="first_name" class="required" value=""></div>

<div class="row2"><label>* Last Name:</label> <input type="text"
	name="last_name" class="required" value=""></div>


<div class="row2"><label>*Email:</label> <input type="text" name="email" class="required email"
	value=""></div>

<div class="row2"><label>*Country:</label> <input type="text"
	name="country" value=""></div>

<div class="row2"><label>City:</label> <input type="text" name="city" class="required"
	value=""></div>

<div class="row2"><label> Address:</label> <input type="text"
	name="address" class="" value=""></div>

<div class="row2 form-actions">
<input type="submit"
	class="btn btn-primary save"> <input type="reset" class="btn reset">
	<button class="btn close">Close</button>
	</div>


</fieldset>

</form>
</div>
</div>


</div>

<div id="debug" style="display: none;"></div>

<script>
  var ContactWidget = function(){
	var self = this;

	self.context = {url_list:'index.php',order:'<?=$_REQUEST['order']?>', direction:'<?=$_REQUEST['direction']?>'}
	self.contacts = []
	  
	self.list();

	$('.add').bind('click', function(e){
        self.editContact($(this));

        e.preventDefault();
  })
  }

  ContactWidget.prototype.alert = function (msg) {
	    console.log(msg);
	    //alert(msg);

	    $('#status').html('<div class="alert alert-info">'+msg+'</div>');
	    
	}


  ContactWidget.prototype.editContact = function($target){

	  var self = this;
	  
	  var view = $('#contactEditTmpl .content').clone();

	  var contact = $target.data('params');
	  if(contact){
		  $(view).find('#title').html('Edit Contact');	  
	  $(view).find(':input[name="id"]').val(contact.id);
	  $(view).find(':input[name="first_name"]').val(contact.first_name);
	  $(view).find(':input[name="last_name"]').val(contact.last_name);
	  $(view).find(':input[name="email"]').val(contact.email);
	  $(view).find(':input[name="country"]').val(contact.country);
	  $(view).find(':input[name="city"]').val(contact.city);
	  $(view).find(':input[name="address"]').val(contact.address);
	  
	  }else{
		  $(view).find('#title').html('Add Contact');	  
	  }

	  
	  
	  //$(view).find('.cancel').data('params', contact);

	  $('#editArea').html('');
	  $(view).appendTo($('#editArea'));


	  $('#editArea form').submit(function(e) { 

		  e.preventDefault();
		 // alert('#editArea form sumit..');
		  //var params = {action:'actionContactEdit',format:'json'}
        //  params = $.extend(params, $(this).serializeArray())
          
         var params = $(this).serialize()
      
		    console.log("ContactWidget.prototype.list context:" + JSON.stringify(self.context));

		    console.log("ContactWidget.prototype.list params:" + JSON.stringify(params));

		    jQuery.ajax(
		        {
		            type:'POST',
		            url:self.context.url_list,
		            data:params,
		            dataType:'json',
		            async:false,
		            //contentType: 'application/json; charset=utf-8',
		            success:function (data) {
		                console.log("ContactWidget.prototype.list response:" + JSON.stringify(data));

		                if (data.s == 1) {
			                if(data.contact && data.contact.status == 1){
	                            self.addToContacts(data.contact);
			                }
	                     }

		                $('#editStatus ul').html('');
	                	$('#editStatus').show();
	                	

		                if (data.m) {
		                    self.alert(data.m);
                            $('#editStatus ul').append("<li>"+data.m+"</li>");
		                }else  if(data.contact && data.contact.status == 1){
		                	 $('#editStatus ul').append("<li>Record updated!</li>");
		                }

		                if(data.contact && data.contact.errors){

		                	$('#editStatus ul').html('');
		                	$('#editStatus').show();
		                	
                            $.each(data.contact.errors, function(ei,er){
                             $('#editStatus ul').append("<li>"+er+"</li>");

                             console.log("ContactWidget.prototype.list err:" + er);
                             
                          });
		                }

		                //self.hasResult()
		            }, error:function () {
		            self.alert("Error saving contacts!")

		        }

		        });

          
          return false;
	  })
	  
	  $('#editArea').find('.reset').bind('click', function(e){
		  $('#editArea').find('#title').html('Add Contact');	  
	  })
	  
	  $('#editArea').find('.close').bind('click', function(e){
		  $('#editArea').find('#title').html('Add Contact');	  
		  $('#editArea').html('');	  
	  })

	  //$(view).appendTo($('#debug'));
	  console.log("ContactWidget.prototype.editContact contact: "+JSON.stringify(contact));
	  console.log("ContactWidget.prototype.editContact contact: "+$(view).html());
	  
	  

  }


  ContactWidget.prototype.removeContact = function($target){

	  var self = this;
	  
	  
	  var contact = $target.data('params');
	  if(!contact){
		return;
	  }

	  $('#editArea').html('');

	  var params = {action:'actionContactRemove', format:'json', id:contact.id}
      
		    console.log("ContactWidget.prototype.list context:" + JSON.stringify(self.context));

		    console.log("ContactWidget.prototype.list params:" + JSON.stringify(params));

		    jQuery.ajax(
		        {
		            type:'POST',
		            url:self.context.url_list,
		            data:params,
		            dataType:'json',
		            async:false,
		            //contentType: 'application/json; charset=utf-8',
		            success:function (data) {
		                console.log("ContactWidget.prototype.list response:" + JSON.stringify(data));

		                if (data.s == 1) {
			                 $('.contact_'+contact.id).remove();
			                 self.contacts.remove(contact)
			            }


		                if (data.m) {
		                    self.alert(data.m);
                            $('#status').append(data.m);
		                }
			            

		               
		            }, error:function () {
		            self.alert("Error removing contact!")

		        }

		        });

  }



  

  ContactWidget.prototype.addToContacts = function(contact){

	  var self = this;
	  var view = $('#contactViewTmpl tr').clone();

	  $(view).addClass('contact_'+contact.id);
	  $(view).find('.id').html(contact.id);
	  $(view).find('.first_name').html(contact.first_name);
	  $(view).find('.last_name').html(contact.last_name);
	  $(view).find('.email').html(contact.email);
	  $(view).find('.country').html(contact.country);
	  $(view).find('.city').html(contact.city);
	  $(view).find('.address').html(contact.address);
	  
	  $(view).find('.edit').data('params', contact);

	  $(view).find('.edit').bind('click', function(e){
		  e.preventDefault();
          self.editContact($(this));

          return false;
            
	  })
	  
	  
	  
	  $(view).find('.remove').data('params', contact);


	  $(view).find('.remove').bind('click', function(e){
		  e.preventDefault();
          self.removeContact($(this));

          return false;
	  })
	  
	  $(view).appendTo($('#listArea tbody'));

	  $(view).appendTo($('#debug'));
	  console.log("Appending contact: "+JSON.stringify(contact));
	  console.log("Appending contact: "+$(view).html());
	  
	  self.contacts.push(contact);
  }
	
  ContactWidget.prototype.list = function(){

	  var self = this;

	   var params = {action:'actionContacts',format:'json', order:self.context.order, direction:self.context.direction}

	    console.log("ContactWidget.prototype.list context:" + JSON.stringify(self.context));

	    console.log("ContactWidget.prototype.list params:" + JSON.stringify(params));

	    jQuery.ajax(
	        {
	            type:'GET',
	            url:self.context.url_list,
	            data:params,
	            dataType:'json',
	            async:false,
	            //contentType: 'application/json; charset=utf-8',
	            success:function (data) {
	                console.log("ContactWidget.prototype.list response:" + JSON.stringify(data));

	                if (data.s == 1) {
                       self.contacts = data.contacts

                       if(self.contacts && self.contacts.length >0){
                            $.each(self.contacts , function(ci, contact){
                                self.addToContacts(contact);
                            });
                       }
	                }

	                if (data.m) {
	                    self.alert(data.m);
	                }

	                //self.hasResult()
	            }, error:function () {
	            self.alert("Error fetching contacts!")

	        }

	        });
		  
  }
</script>

<script>
  $(function(){
     var contactWidget = new ContactWidget();
     
  })

</script>








