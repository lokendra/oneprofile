<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Oneprofile.com</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">


<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/bootstrap-responsive.css" rel="stylesheet">


<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/boostrap.min.js"></script>
<script type="text/javascript" src="../js/common.js"></script>


</head>

<body data-spy="scroll" data-target=".subnav" data-offset="50"
	style="padding-top: 30px;">


<!-- Navbar
    ================================================== -->
<div class="navbar navbar-inverse navbar-fixed-top">
<div class="navbar-inner">
<div class="container">
<button type="button" class="btn btn-navbar" data-toggle="collapse"
	data-target=".nav-collapse"><span class="icon-bar"></span> <span
	class="icon-bar"></span> <span class="icon-bar"></span></button>
<div class="nav-collapse collapse">

<ul class="nav">
	<li><a href="index.php">
	<h1
		style="font-family: 'Quando', serif; font-size: 2.5em; margin-bottom: 0px; margin-top: 0px;">Oneprofile.com</h1>
	</a></li>
</ul>
<ul class="nav pull-right">
<li class="divider-vertical"></li>
	<li class=""><a href="#">
	<h5>Welcome <?=$sessionUser->email?></h5>
	</a></li>
<?php if($sessionUser && $sessionUser->status == User::$STATUS_OK) {?>
<li class="divider-vertical"></li>
	<li class=""><a href="index.php?action=actionLogout">
	<h5>Logout</h5>
	</a></li>
	<?php }?>

	<li class="divider-vertical"></li>
	<li class=""><a href="#">
	<h5>Industry</h5>
	</a></li>

	<li class="divider-vertical"></li>
	<li class="dropdown"><a class="dropdown-toggle" href="#"
		data-toggle="dropdown">
	<h5>Company <b class="caret"></b></h5>
	</a>
	<ul class="dropdown-menu">
		<li><a href="#">News</a></li>
		<li class="divider"></li>
		<li><a href="#">Contact US</a></li>
	</ul>
	</li>

</ul>

</div>
</div>
</div>
</div>


<div class="container">
<div style="margin-top: 50px;"><?php include $action;?></div>


</div>











<!-- Footer
    ================================================== -->
<footer class="footer"
	style="margin-top: 0px; padding-bottom: 15px; padding-top: 0px;">

<div class="row-fluid" style="height: 15px; margin-top: 21px;">

<div class="span7 pull-right"><font color="white"
	style="padding-left: 35%;">&#169; All Rights Reserved, Oneprofile.com |
enquiry@oneprofile.com</font></div>
</div>


</footer>

<div class="alert alert-info"><?php 
echo '<hr/>';
echo '<br>request :';
var_dump($_REQUEST);
echo '<br>post :';
var_dump($_POST);
echo '<br>action :';
var_dump($action);

echo '<br>session :';
var_dump($_SESSION);
?></div>





</body>
</html>
