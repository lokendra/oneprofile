-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.7-rc


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema oneprofile
--

CREATE DATABASE IF NOT EXISTS oneprofile;
USE oneprofile;

--
-- Definition of table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
CREATE TABLE `profiles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `owner_id` bigint(20) unsigned NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index_2` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profiles`
--

/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` (`id`,`owner_id`,`first_name`,`last_name`,`email`,`phone`,`address`,`country`,`city`) VALUES 
 (15,1,'Lokeshz','Kumar','lokendra3777@gmail.com',NULL,'','India',''),
 (17,1,'Lokesh2','Kumar','lokendra3777@gmail.com4',NULL,'','India',''),
 (19,1,'aLokesh','Lodha','lokendra3777@gmail.coma',NULL,'','Australia','');
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;


--
-- Definition of table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `email` varchar(255) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `password` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `status` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`,`email`),
  KEY `Index_2` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`email`,`id`,`password`,`date_created`,`status`) VALUES 
 ('lokendra3777@gmail.com',1,'password','2013-03-14 00:00:00',NULL),
 ('lokendra3777@gmail.com',2,'password','2013-03-14 00:00:00',NULL),
 ('lokendra3777@gmail.com',3,'password','2013-03-14 00:00:00',NULL),
 ('lokendra3777@gmail.com',4,'password','2013-03-14 00:00:00',NULL),
 ('lokendra3777@gmail.com',5,'password','2013-03-14 00:00:00',NULL),
 ('lokendra3777@gmail.com',6,'password','2013-03-14 00:00:00',NULL),
 ('lokendra3777@gmail.com',7,'password','2013-03-14 00:00:00',NULL),
 ('lokendra3777@gmail.com',8,'password','2013-03-14 00:00:00',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
