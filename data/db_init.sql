CREATE DATABASE IF NOT EXISTS oneprofile CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE USER 'oneprofile'@'localhost' IDENTIFIED BY 'oneprofile123';


CREATE USER 'oneprofile'@'%' IDENTIFIED BY 'oneprofile123';

GRANT ALL PRIVILEGES ON oneprofile.* TO 'oneprofile'@'localhost'   WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON oneprofile.* TO 'oneprofile'@'%'   WITH GRANT OPTION;


