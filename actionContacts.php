<?php

header('Content-type: application/json');

$contactModel = new Contact();
$contacts = $contactModel->findAll($_REQUEST, $_REQUEST);

$status = array('s'=>1);
$status['contacts'] = $contacts;


$resultJson = htmlspecialchars(json_encode($status), ENT_NOQUOTES);
echo $resultJson;
return;