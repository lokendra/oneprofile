<?php
class Model{
	static $STATUS_ERROR = -1;
    static $STATUS_OK = 1;
    
    public $status;
	public $errors;
	
	public function hasErrors(){
		return ($this->errors && count($this->errors)>0)?true:false;
	
	}
	
	public function addError($msg){
		if(!$this->errors){
			$this->errors = array();
		}
		
		
		$this->errors[] = $msg;
	}
}