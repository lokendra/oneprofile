<?php

class User extends Model{

	public $email;

	public $accessRules = array(
      'admin'=>array('admin'),
      '@'=>array('actionHomeView','actionLogout2'),
      '*'=>array('actionLoginView','actionLogin','actionLogout','actionContacts','actionContactEdit','actionContactRemove','actionContactUpdate')
	);

	public function executeAction(){
		$user = User::getSessionUser();

		$action = isset($_REQUEST['action'])?$_REQUEST['action']:'actionHomeView';
		//var_dump($user);
		//var_dump($action);
		
		
		if(in_array($action, $this->accessRules['admin'])){
			 
			if( $user->status != self::$STATUS_OK){

				return 'actionLoginView';
			}else{
				
				//@todo not checking roles for now
				return $action;
			}
		}

		if(in_array($action, $this->accessRules['@'])){
			 
			if( $user->status != self::$STATUS_OK){

				return 'actionLoginView';
			}else{
				return $action;
			}
		}

		if( in_array($action, $this->accessRules['*']) ){
			return $action;
		}

		//@todo - any resource rules not defines might be allowed/disalloed by default
		return 'actionLoginView';
	}

	public static function login($email, $password){
		$user = new User();

		if(!$email || !$password){
			$user->status = self::$STATUS_ERROR;
			$user->addError("Authentication error. Please enter correct credentials!");
				
			return $user;
		}

		$db = DbService::getInstance();
		$stmt = $db->prepare("select * from users where email = :email and password =:password");
		$stmt->bindValue(':email', $email, PDO::PARAM_STR);
		$stmt->bindValue(':password', $password, PDO::PARAM_STR);

		$stmt->execute();
		//$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if($row){
			$_SESSION['userId'] = $row['id'];
			$_SESSION['user'] = $row;
				
			$user = new User();
			$user->email = $row['email'];
				
			$user->status = self::$STATUS_OK;
				
		}else{
			$user->status = self::$STATUS_ERROR;
			$user->addError("Authentication error. Please enter correct credentials!");
		}

		return $user;



	}


	public static function getSessionUser(){
		$user = new User();

		if(!isset($_SESSION['userId'])){
			$user->status = self::$STATUS_ERROR;
			$user->addError("Not logged in!");
				
			return $user;
		}

		$db = DbService::getInstance();
		$stmt = $db->prepare("select * from users where id = :id");
		$stmt->bindValue(':id', intval($_SESSION['userId']), PDO::PARAM_INT);
		$stmt->execute();
		//$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if($row){
			$_SESSION['userId'] = $row['id'];
			$_SESSION['user'] = $row['user'];
				
			$user = new User();
			$user->email = $row['email'];
				
			$user->status = self::$STATUS_OK;
				
		}else{
			$user->status = self::$STATUS_ERROR;
			$user->addError("Authentication error. Please enter correct credentials!");
		}

		return $user;



	}


	public function configure(){
		$users = array(
		array('email'=>'lokendra3777@gmail.com','password'=>'password')
		);


		$db = DbService::getInstance();
		$stmt = $db->prepare("INSERT INTO users(email,password,date_created) VALUES (:email,:password,:date_created)");

		foreach($users as $user) {
			$stmt->bindParam(':email', $user['email'], PDO::PARAM_STR);
			$stmt->bindParam(':password', $user['password'], PDO::PARAM_STR);
			$stmt->bindParam(':date_created', date('Y-m-d', time()), PDO::PARAM_STR);
			//$stmt->bindParam(':status', 1, PDO::PARAM_INT);

			$stmt->execute();
		}
	}

}
